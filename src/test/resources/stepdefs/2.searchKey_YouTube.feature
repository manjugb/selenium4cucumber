@web
Feature: As a Youtube user i will go and search my favorite singer
Scenario Outline: Validate search key on Firefox

Given I Go to "<url>" on "<Browser>"
Then I Enter Keyword to Search "<key>"
Then I Wait for some Time
Then I Click Search Button
Then I Wait for some Time
And I Verify Singer "<expkey>"

Examples:
|url|Browser|key|expkey|
|https://www.youtube.com|Firefox|Mano|Mano|
|https://www.youtube.com|Firefox|MS Subbulakshmi|MS Subbulakshmi|
|https://www.youtube.com|Firefox|Saluri Koteswara Rao|Saluri Koteswara Rao|
|https://www.youtube.com|Firefox|chiranjeevi|Chiranjeevi|
|https://www.youtube.com|Firefox|sunitha|Sunitha|
|https://www.youtube.com|Firefox|Ghantasala|Ghantasala|
|https://www.youtube.com|Firefox|suchitra|Suchitra|

Scenario: Validate search key  Close Firefox
Then I Close Browser

Scenario Outline: Validate search key on Chrome

Given I Go to "<url>" on "<Browser>"
Then I Wait for some Time
Then I Enter Keyword to Search "<key>"
Then I Wait for some Time
Then I Click Search Button
Then I Wait for some Time
And I Verify Singer "<expkey>"

Examples:
|url|Browser|key|expkey|
|https://www.youtube.com|Chrome|Mano|Mano|
|https://www.youtube.com|Chrome|ms subbalakshmi|MS Subbulakshmi|
|https://www.youtube.com|Chrome|Saluri Koteswara Rao|Saluri Koteswara Rao|
|https://www.youtube.com|Chrome|chiranjeevi|Chiranjeevi|
|https://www.youtube.com|Chrome|Singer Sunitha|Singer Sunitha|
|https://www.youtube.com|Chrome|Ghantasala|Ghantasala|
|https://www.youtube.com|Chrome|suchitra|Suchitra|

Scenario: Validate search key  Close Chrome
Then I Close Browser
