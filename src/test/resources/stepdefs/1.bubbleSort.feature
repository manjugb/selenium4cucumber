@unit
Feature: Insertion Sort Technique
@insertionsort
Scenario Outline: Validate Insertion Algorithm

Given a SortTechniqes I just turned on
Then With an array "<list1>" Sorted as "<list2>"

Examples:
|list1|list2|
|4,3,2,1|1,2,3,4|
|4,3,2,8|2,3,4,8|
|3,4,9,1,2,0|0,2,1,3,4,9|
|3,4,9,1,2,0|0,1,2,3,4,9|
|0|0|
|3.5|4.5|
|dfd|dsfds|

  