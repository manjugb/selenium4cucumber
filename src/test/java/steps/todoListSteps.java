package steps;

import static org.junit.Assert.assertEquals;
import static utils.Constants.myLogger;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import units.SortTechniques;
import units.TodoList;

public class todoListSteps {
	private TodoList todolist;
	@org.junit.Before
	public void before() {
		
	}
	
	@org.junit.After
	public void after() {
		
	}
	
	@Given("^a Todo List I just turned on$")
    public void setup() {
		todolist = new TodoList("My Morning Routine");
		myLogger.info("Todo List Started ");
		
    }
	
	/*@Then("^a Todo List I just turned stop$")
	public void tearDown() throws InterruptedException {
		wait(10);
		myLogger.info("Todo List Ended ");
	}*/
	
	@Then("^I add Task \"([^\"]*)\"$")
	public void test_addtask(String taskName) throws Throwable{
		todolist.addTask(taskName);
		int count = todolist.getCount();
		int i=0;
		assertEquals(i+1, count);
		//assertEquals(0,count);
	}
	@Then("I Can visible in the List")
	public void test_display() throws Throwable{
		todolist.display();
		
	}

}
