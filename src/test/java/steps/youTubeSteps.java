package steps;

import static utils.Constants.myLogger;
import static utils.Swd.wd;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pageObjects.youTube;

import testActions.youTubeTest;
import utils.Swd;
import utils.Utils;

public class youTubeSteps {
	
	static WebDriverWait wait;
	private youTubeTest ytb = new youTubeTest();
	

	@Given("^I Go to \"([^\"]*)\" on \"([^\"]*)\"$")
	// Given("^I Go to \"([^\"]*)\" on \"([^\"]*)\"$", (String url,String browser)
	// -> {
	public void open_browser(String url, String browser) throws Throwable {
		Swd.getDriver(url, browser);
		wd.get(url);
		myLogger.info(" youtube home page opened ");

	}

	@Then("^I Enter Keyword to Search \"([^\"]*)\"$")
	public void enter_key(String key) throws Throwable {
		// test.log(Status.PASS, "User Enter the keyword to search");
		ytb.enter_key(key);
		myLogger.info(" Guest user enter "+key+" the keyword to search ");

	}

	@Then("I Press Enter$")
	public void pres_enter() throws Throwable {
		ytb.press_enter();
		myLogger.info(" Guest user enter the key in order to press");
	}

	@Then("^I Click Search Button$")
	public void clk_Search() throws Throwable {

		ytb.clk_searchbut();
		myLogger.info("User click on search button ");
	}

	@Then("^I Click Search Button Chrome$")
	public void clk_Search_Chrome() throws Throwable {
		ytb.clk_searchbut_chrome();
                myLogger.info("User click on search button ");
	}

	@Then("^I Verify Singer \"([^\"]*)\"$")
	public void verifytext(String expText) throws Throwable {
		ytb.verifysearchkey(expText);
		myLogger.info("Finding user key word ");
	}

	@Then("^I Close Browser$")
	public void close_br() throws Throwable {
		ytb.destroyDriver();
            myLogger.info("Close the browser ");


	}

	@Then("^I Wait for some Time$")
	public void waitfor() {
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
