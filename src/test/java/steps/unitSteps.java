package steps;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static utils.Constants.myLogger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import units.SortTechniques;



public class unitSteps {
	
	private SortTechniques sortTest;
	@org.junit.Before
	public void before() {
		
	}
	
	@org.junit.After
	public void after() {
		
	}
	
	
	
	
	
	
	@Given("^a SortTechniqes I just turned on$")
    public void setup() {
		sortTest = new SortTechniques();
		myLogger.info("Sort Techniqes Started ");
		
    }
	
	@Given("^a SortTechniqes I just turned stop$")
	public void tearDown() {
		
		myLogger.info("Sort Techniqes Ended ");
	}
	
	@Then("With an array \"([^\"]*)\" Sorted as \"([^\"]*)\"$")
	public void with_a_custom_parameter_type(String actInt,String expInt) {
		
		try {
		List<Integer> intList = Stream.of(actInt.split(","))
		        .map(Integer::valueOf)
		        .collect(Collectors.toList());
		
		List<Integer> expintList = Stream.of(expInt.split(","))
		        .map(Integer::valueOf)
		        .collect(Collectors.toList());
		
		if(sortTest.insertSort(intList).equals(expintList)){
		
        assertEquals(expintList, sortTest.insertSort(intList));
		}else {
			 assertNotEquals(expintList, sortTest.insertSort(intList));
		}
		}
		catch(Exception NumberFormatException) {
			System.out.println("Not valid Array");
		}

    
    	
    }
		
	
	@And("^I Enter Values to Sort \"([^\"]*)\" Equals \"([^\"]*)\"$")
	public void testSort(int[] testData,int[] expData) throws Throwable{
		myLogger.info("Bubble Sort Execution Started ");
		
	int[] actnum = sortTest.bubbleSort1(testData);
	
	assertEquals(actnum, expData);
	myLogger.info("Bubble Sort Execution Ended ");
}
	
	
	
}
