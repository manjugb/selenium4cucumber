package units;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/* Todo list models the attributes 
 * and the operations we perform
 * 
 */

public class TodoList {
	private String topic;
	//private ArrayList<String> tasks;
	private HashMap<String,Boolean> tasks;

	/*
	 * To do list instantiates a TodoList with provided string as a topic for the
	 * list and allocates the memory for the list of tasks
	 * 
	 * @perform the topic for which list is created for
	 */
	public TodoList(String topic) {
		this.topic = topic;
		this.tasks = new HashMap<String,Boolean>();
	}

	/* add task is appends the provided String at the end of the list
	 * @param taskName the description of the tasks sould be added
	 * 
	 */
	public void addTask(String taskName) {
	
		
		this.tasks.put(taskName,true);
		
	}
    
	/* displays the topic of the list
	 * 
	 */
	
	public void display() {
		if(this.tasks.size() == 0) {
			System.out.println("Our Todo list for"+topic+ "is currently Empty");
		}else {
			System.out.println("Here is the list for "+ topic);
			for(Entry<String,Boolean> task:this.tasks.entrySet()) {
				if(task.getValue()) {
				System.out.println("Task" + task.getKey() + "is completed");
			}else {
				System.out.println("Task" + task.getKey() + "is not done");
			}
		}
		}		
	}
	
	public void markAsDone(String taskName) {
		System.out.println("Marking" + taskName + "as completed");
		if(this.tasks.containsKey(taskName)) {
			this.tasks.put(taskName, true);
		}else {
			System.out.println("no such task");
		}
	}
	
	public int getCount() {
		return tasks.size();
		
	}

 /* 
  * remove removes a task at the provided index
  * @param i the index of the task to remove	
  */
	
	public void removeTask(String taskName) {
	if(this.tasks.containsKey(taskName)) {
	 System.out.println("Removing" +taskName);
	this.tasks.remove(taskName);
	}else {
		System.out.println("No such Task");
	}
	}
	
	
			
			
		
	
	
 /*	rename renames the task at the provided index
  * 
  * @param i the index of the task to rename
  * @param newtask the new description for the task
  */
	public void renameTask(String oldTask,String newTask) {
		/*try {
			this.tasks.se(i, newTask);
		}catch(IndexOutOfBoundsException e) {
			System.out.println("Could not rename at this Index!");
		}
	}*/
		System.out.println("Renaming" +oldTask+ "to" +newTask);
		if(this.tasks.containsKey(oldTask)) {
			this.tasks.put(newTask, this.tasks.get(oldTask));
			this.tasks.remove(oldTask);
		}
		else {
			System.out.println("no such task!");
		}
	}
}
