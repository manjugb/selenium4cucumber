package stepdefs;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static utils.Constants.myLogger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.bytebuddy.build.HashCodeAndEqualsPlugin.Sorted;
import units.*;
import static java.util.Arrays.asList;



public class unitSteps {
	
	private SortTechniques sortTest;
	@Before
	public void before() {
		
	}
	
	@After
	public void after() {
		
	}
	
	
	
	
	
	
	@Given("^a SortTechniqes I just turned on$")
    public void setup() {
		sortTest = new SortTechniques();
		myLogger.info("Sort Techniqes Started ");
		
    }
	
	@Given("^a SortTechniqes I just turned stop$")
	public void tearDown() {
		
		myLogger.info("Sort Techniqes Ended ");
	}
	
	@Then("With an array \"([^\"]*)\" Sorted as \"([^\"]*)\"$")
	public void with_a_custom_parameter_type(String actInt,String expInt) {
		
		try {
		List<Integer> intList = Stream.of(actInt.split(","))
		        .map(Integer::valueOf)
		        .collect(Collectors.toList());
		
		List<Integer> expintList = Stream.of(expInt.split(","))
		        .map(Integer::valueOf)
		        .collect(Collectors.toList());
		
		if(sortTest.insertSort(intList).equals(expintList)){
		
        assertEquals(expintList, sortTest.insertSort(intList));
		}else {
			 assertNotEquals(expintList, sortTest.insertSort(intList));
		}
		}
		catch(Exception NumberFormatException) {
			System.out.println("Not valid Array");
		}

    
    	
    }
		
	
	@And("^I Enter Values to Sort \"([^\"]*)\" Equals \"([^\"]*)\"$")
	public void testSort(int[] testData,int[] expData) throws Throwable{
		myLogger.info("Bubble Sort Execution Started ");
		
	int[] actnum = sortTest.bubbleSort1(testData);
	
	assertEquals(actnum, expData);
	myLogger.info("Bubble Sort Execution Ended ");
}
	
}
