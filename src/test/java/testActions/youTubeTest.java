package testActions;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pageObjects.youTube;
import utils.Swd;

public class youTubeTest extends Swd {
	
	
	

	private youTube ytb = new youTube();

	public youTubeTest() {
		this.ytb = new youTube();
		PageFactory.initElements(wd, ytb);
	}

	// Enter Keyword
	public void enter_key(String keyword) throws Throwable {

		if(ytb.elmSearchBar.getText().isEmpty()) {
		  ytb.elmSearchBar.sendKeys(keyword);
		  }
		else {
			ytb.elmSearchBar.clear();
			ytb.elmSearchBar.sendKeys(keyword);
			
		}

	}

	public void press_enter() throws Throwable {
		ytb.elmSearchBar.sendKeys(Keys.ENTER);
	}

	// Click on Search Button
	public void clk_searchbut() throws Throwable {

		ytb.elmClickSearch.click();

	}

	public void clk_searchbut_chrome() throws Throwable {

//elmClickSearch_chrm.click();

		Actions action = new Actions(wd);
		action.moveToElement(ytb.elmClickSearch_chrm).perform();
		ytb.elmClickSearch_chrm.click();

	}

	public void verifysearchkey(String text)
	{       
		List<WebElement> list = wd.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		if(list.size() > 0) {
		assertTrue("Text not found!", list.size() > 0);
		for(int i=0;i <= 5;i++) {
		System.out.println(list.get(i).getText());}
		}else {
			assertFalse("Text not found!", list.size() > 0);
		}
		//wd.getPageSource().contains("Text which you looking for");
	}

	

	
}
